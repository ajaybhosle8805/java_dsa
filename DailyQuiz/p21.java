
class SwitchDemo{

	public static void main(String [] args){
	
		int x=1;

		switch(x){
		
			case 1+1: 
				System.out.println("0");
				break;

			case 1*1+0:
				System.out.println("1");
				break;

			case 3/4:
				System.out.println("2");
				break;

			default:
				System.out.println("All");
		}
	}
}
