import java.util.*;

class Input{

	public static void main(String [] args){
	
		Scanner sc=new Scanner(System.in);

		String name=sc.next();  // name="Ajay"
		int y=sc.nextInt();    // y=5

		System.out.println(name + y);
	}
}
