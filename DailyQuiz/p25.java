
class MethodDemo{

	public static void main(String [] args){
	
		MethodDemo obj=new MethodDemo();
		obj.fun("A");
		obj.fun(20.5f);
		obj.fun(-3456.0001);
	}

	void fun(double x){
	
		System.out.println(x);
	}
}
