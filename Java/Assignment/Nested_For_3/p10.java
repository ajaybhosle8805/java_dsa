/*
C2W1 	  C2W2	   C2W3	
C2W1 	  C2W2	   C2W3	
C2W1 	  C2W2	   C2W3	
*/

class Solution10{

	public static void main(String[] args){
	
		int rows=6;
		
		for(int i=1;i<=rows;i++){
		
			int x=rows;
			int ch=64+rows;
			
			for(int j=1;j<=rows;j++){
				
				if(j%2==1){

					System.out.print((char)ch +" ");
				}else{
				
					System.out.print(x +" ");

				}
				ch--;
				x--;
				
			}

			System.out.println();
		}
	}
}
