/*
C2W1 	  C2W2	   C2W3	
C2W1 	  C2W2	   C2W3	
C2W1 	  C2W2	   C2W3	
*/

class Solution9{

	public static void main(String[] args){
	
		int rows=3;
		int x=1;

		for(int i=1;i<=rows;i++){
		
			int num=rows;
			int ch=65+rows-1;
			
			for(int j=1;j<=rows;j++){
				
				System.out.print(x*x + ""+ (char)ch-- + ""+ num + " ");
				num--;
				x++;
			}

			System.out.println();
		}
	}
}
