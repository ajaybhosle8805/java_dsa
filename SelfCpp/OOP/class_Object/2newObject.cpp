#include<iostream>

class Company{

	int countEmp=500;
	std::string name="IBM";

	public:

	Company(){
	
		std::cout<<"In company Constructor"<<std::endl;

	}

	void compInfo(){
	
		std::cout<<countEmp<<std::endl;
		std::cout<<name<<std::endl;
	}
};

class Employee{

	int empid=10;
	float empsal=95.00f;

	public:

	Employee(){
	
		std::cout<<"In Employee Constructor"<<std::endl;
	}

	void empInfo(){
	
		Company *obj=new Company();
		obj->compInfo();

		std::cout<<empid <<std::endl;
		std::cout<<empsal<<std::endl;
	}
};

int main(){

	
	Employee *emp=new Employee();
	//Employee emp;
	//(*emp).empInfo();
	emp->empInfo();
	
	return 0;

}
